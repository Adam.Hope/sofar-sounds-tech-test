// @flow
import type { StateType } from "../types/types";

export const initialState: StateType = {
  cities: [],
  events: [],
  filters: {
    city: "",
    date: ""
  }
};

export const reducer = (state: StateType, action: Object): StateType => {
  switch (action.type) {
    case "updateEventList": {
      return {
        ...state,
        events: action.events
      };
    }
    case "updateCountryList": {
      return {
        ...state,
        cities: action.cities
      };
    }
    case "updateFilters":
      return {
        ...state,
        filters: {
          city: action.city,
          date: action.date
        }
      };
    case "resetFilters":
      return {
        ...state,
        filters: {
          ...initialState.filters
        }
      };
    default:
      throw new Error();
  }
};
