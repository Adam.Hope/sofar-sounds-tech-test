// @flow
type ApiConfig = {
  eventsUrl: string
};

const apiConfig: ApiConfig = {
  eventsUrl: "https://app.staging.sofarsounds.com/api/v1/events"
};

export default apiConfig;
