// @flow
import type { EventListType } from "../types/types";
import { isSameDay } from "date-fns";

export const filterByCity = (
  events: EventListType,
  city: string
): EventListType =>
  city ? events.filter(event => event.city_slug === city) : events;

export const filterByDate = (
  events: EventListType,
  date: string
): EventListType =>
  date
    ? events.filter(event =>
        isSameDay(new Date(event.start_time), new Date(date))
      )
    : events;
