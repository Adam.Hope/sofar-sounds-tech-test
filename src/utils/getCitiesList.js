// @flow
import type { EventListType, CitiesListType } from "../types/types";

export default (events: EventListType): CitiesListType =>
  events.reduce((cities, event) => {
    if (cities.find(item => item.city === event.city)) {
      return cities;
    }
    return [
      ...cities,
      {
        city: event.city,
        cityId: event.city_id,
        citySlug: event.city_slug
      }
    ];
  }, []);
