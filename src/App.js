// @flow

import React, { useEffect, useReducer } from "react";
import Header from "./components/header/header";
import FindEvents from "./components/find-events/find-events";
import EventsList from "./components/events-list/events-list";
import { ThemeProvider } from "styled-components";
import GlobalStyles from "./styles/global";
import theme from "./styles/theme";
import { reducer, initialState } from "./state/reducer";
import api from "./config/api";
import { filterByCity, filterByDate } from "./utils/filter";
import getCitiesList from "./utils/getCitiesList";

import type { EventListType } from "./types/types";

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    async function fetchData() {
      try {
        const res = await fetch(api.eventsUrl);
        const events: Promise<EventListType> = await res.json();
        dispatch({
          type: "updateEventList",
          events
        });
      } catch (err) {
        console.error("there was an issue fetching the data", err);
      }
    }
    if (!state.events.length) {
      fetchData();
    } else {
      if (!state.cities.length) {
        dispatch({
          type: "updateCountryList",
          cities: getCitiesList(state.events)
        });
      }
    }
  });

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Header />
      <FindEvents cities={state.cities} dispatch={dispatch} />
      <EventsList
        events={filterByDate(
          filterByCity(state.events, state.filters.city),
          state.filters.date
        )}
        filters={state.filters}
      />
    </ThemeProvider>
  );
}

export default App;
