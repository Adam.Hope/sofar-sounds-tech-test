import styled from "styled-components";
import { ReactComponent as Logo } from "./../../assets/logo.svg";

export const Header = styled.section`
  width: 100%;
  height: 400px;
  background-image: url("/header.jpg");
  background-size: cover;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.h1`
  color: ${({ theme }) => theme.color.text.primary};
`;

export const SofarLogo = styled(Logo)`
  position: absolute;
  top: 28px;
  width: 100%;
  height: 42px;
`;
