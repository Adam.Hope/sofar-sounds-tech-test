import React from "react";

import { Header, Title, SofarLogo } from "./header.styles";

export default props => {
  return (
    <Header>
      <SofarLogo />
      <Title>Find The Show</Title>
    </Header>
  );
};
