// @flow
import React from "react";
// import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import { FormControlContainer, Label } from "./cities-select.styles";

import type { CitiesListType } from "../../../types/types";

type Props = {
  labelId: string,
  label: string,
  id: string,
  value: string,
  cities: CitiesListType,
  onChange: any => void
};

export default (props: Props) => {
  return (
    <FormControlContainer variant="outlined">
      <Label id={props.labelId} shrink={true}>
        {props.label}
      </Label>
      <Select
        value={props.value}
        labelId={props.labelId}
        id={props.id}
        onChange={props.onChange}
      >
        {props.cities &&
          props.cities.map(city => (
            <MenuItem key={`${city.cityId}`} value={city.citySlug}>
              {city.city}
            </MenuItem>
          ))}
      </Select>
    </FormControlContainer>
  );
};
