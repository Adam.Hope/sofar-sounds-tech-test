import styled from "styled-components";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";

export const FormControlContainer = styled(FormControl)`
  width: 200px;
  margin-right: 40px !important;

  @media (max-width: 640px) {
    margin-bottom: 10px !important;
  }
`;
export const Label = styled(InputLabel)`
  background-color: ${({ theme }) => theme.color.background.primary};
`;
