// @flow
import React from "react";
import { format } from "date-fns";

import {
  Container,
  EventImage,
  Content,
  BoldText,
  Text
} from "./event-item.styles";

type Props = {
  imageUrl: string,
  dateTime: string,
  city: string
};

export default (props: Props) => {
  const date = new Date(props.dateTime);
  return (
    <Container>
      <EventImage imageUrl={props.imageUrl} />
      <Content>
        <BoldText>{format(date, "EEEE, do LLLL y")}</BoldText>
        <Text>Doors open at {format(date, "HH:mm")}</Text>
        <Text>{props.city}</Text>
      </Content>
    </Container>
  );
};
