import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  margin: 24px 0;
  border: solid 1px ${({ theme }) => theme.color.border.primary};
  border-radius: 2px;
`;

export const EventImage = styled.div`
  width: 100%;
  height: 100px;
  background-image: url("https:${props => props.imageUrl}");
  background-size: cover;
`;

export const Content = styled.div`
  width: 100%;
  height: 100px;
  padding: 10px;
  box-sizing: border-box;
  display-flex;
  flex-direction: column;
  justify-content: space-between;
  text-align: left;
`;

export const BoldText = styled.p`
  font-weight: 600;
  margin: 4px;
`;

export const Text = styled.p`
  font-weight: 400;
  margin: 4px;
`;
