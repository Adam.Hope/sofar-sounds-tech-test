import styled from "styled-components";
import Button from "@material-ui/core/Button";

export const Container = styled.section`
  width: 100%;
  display: flex;
  justify-content: center;
  background-color: ${({ theme }) => theme.color.background.secondary};
`;

export const Content = styled.div`
  width: 900px
  margin: 10px;
  display: grid;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media(max-width: 640px) {
    flex-direction: column;
    justify-content: center; //
    align-items: center;
    margin-bottom: 12px;
    text-align: center;

    && button, div {
      display: block;
    }
    && div {
      width: 100%;
    }
  }
`;

export const Block = styled.div``;

export const SearchButton = styled(Button).attrs({
  variant: "contained"
})`
  background-color: ${({ theme }) => theme.color.button.success} !important;
  color: ${({ theme }) => theme.color.text.primary} !important;
  margin: 0 6px !important;
  text-transform: capitalize !important;
  height: 50px; /* Add to theme */
`;

export const ResetButton = styled(Button)`
  text-transform: capitalize !important;
  height: 50px; /* Add to theme */
`;
