// @flow

import React, { useState } from "react";
import CitySelect from "../shared/city-select/cities-select";
import TextField from "@material-ui/core/TextField";
import {
  Container,
  Content,
  SearchButton,
  ResetButton,
  Block
} from "./find-events.styles";

import type { CitiesListType } from "../../types/types";

type Action = {
  +type: string
};

type Props = {
  cities: CitiesListType,
  dispatch: Action => any
};

export default (props: Props) => {
  const [city, updateCity] = useState("");
  const [date, updateDate] = useState("");
  return (
    <Container>
      <Content>
        <Block>
          <CitySelect
            id="city"
            labelId="city"
            label="Filter By City"
            cities={props.cities}
            value={city}
            onChange={({ target }) => updateCity(target.value)}
          />
          <TextField
            variant="outlined"
            id="date"
            label="Filter By Date"
            type="date"
            onChange={({ target }) => updateDate(target.value)}
            InputLabelProps={{
              shrink: true
            }}
          />
        </Block>
        <Block>
          <ResetButton onClick={() => props.dispatch({ type: "resetFilters" })}>
            Reset Filter
          </ResetButton>
          <SearchButton
            onClick={() =>
              props.dispatch({
                type: "updateFilters",
                city,
                date
              })
            }
          >
            Search
          </SearchButton>
        </Block>
      </Content>
    </Container>
  );
};
