import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
`;
export const Content = styled.div`
  margin: 0 auto;
  padding: 0 10px;
  max-width: 900px;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(260px, 1fr));
  grid-column-gap: 4%;
`;
