// @flow
import React from "react";

import type { EventListType, EventType } from "../../types/types";
import { Container, Content } from "./events-list.styles";

import Event from "../shared/event-item/event-item";

type Props = {
  events: EventListType
};

export default ({ events = [] }: Props) => {
  return (
    <Container>
      <Content data-testid="EventsList">
        {events.length ? (
          events.map((event: EventType) => (
            <Event
              key={`${event.id}`}
              imageUrl={event.image_url}
              dateTime={event.start_time}
              city={event.city}
            />
          ))
        ) : (
          <p>Sorry no events could be found</p>
        )}
      </Content>
    </Container>
  );
};
