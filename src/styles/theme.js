const palette = {
  white: "#ffffff",
  black: "#000000",
  lightGrey: "#F9F9F9",
  midGrey: "#CCCCCC",
  brand: "#10ad52",
  midGreen: "#11AD52"
};

export default {
  color: {
    brand: {
      primary: palette.brand
    },
    text: {
      primary: palette.white,
      body: palette.black
    },
    background: {
      primary: palette.white,
      secondary: palette.lightGrey
    },
    border: {
      primary: palette.midGrey
    },
    button: {
      success: palette.midGreen
    }
  },
  font: {
    family: {
      primary: "'Open Sans', sans-serif"
    }
  },
  size: {
    text: {
      h1: "62px",
      p: "16px"
    }
  }
};
