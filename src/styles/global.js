import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap');

  body {
    margin: 0;
    font-family: 'Open Sans', sans-serif; 
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  h1 {
    font-size: ${({ theme }) => theme.size.text.h1};
    font-weight: 800;
  }

  p {
    font-size: ${({ theme }) => theme.size.text.p};
    font-weight: 400;
  }
`;
