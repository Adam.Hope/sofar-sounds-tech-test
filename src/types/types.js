// @flow
export type FiltersType = {
  city: string,
  date: string
};

export type PriceType = {
  currency: string,
  currency_symbol: string,
  currency_suffix: boolean
};

export type EventType = {
  appliable: boolean,
  arrival_time: string,
  billing_region: string,
  byob: boolean,
  city: string,
  city_id: number,
  city_slug: string,
  closest_station: string,
  confirmable: boolean,
  end_time?: string,
  event_url: string,
  id: number,
  image_url: string,
  location_latitude?: number,
  location_longitude?: number,
  location_name: string,
  pricing: PriceType,
  remaining_spaces: number,
  start_time: string,
  theme: string,
  ticket_limit: number,
  ticket_price: number,
  type: String
};

export type EventListType = Array<EventType>;

export type CityType = {
  cityId: number,
  citySlug: string,
  city: string
};

export type CitiesListType = Array<CityType>;

export type StateType = {
  cities: CitiesListType,
  events: EventListType,
  filters: FiltersType
};
