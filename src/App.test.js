import {
  queryAllByText as queryAllByTextDom,
  findAllByText as findAllByTextDom
} from "@testing-library/dom";
import React from "react";
import { render, fireEvent } from "@testing-library/react";
import App from "./App";
import eventsMock from "./mocks/events";

global.fetch = jest.fn(args => {
  return Promise.resolve({
    json: () => Promise.resolve(eventsMock)
  });
});

jest.mock(
  "./components/shared/city-select/cities-select",
  () => ({ cities, value, onChange, id, labelId }) => {
    return (
      <select
        data-testid="select"
        value={value}
        onChange={() => onChange({ target: { value: "oslo" } })}
      >
        {cities.map(({ cityId, citySlug, city }) => (
          <option key={cityId} value={citySlug}>
            {city}
          </option>
        ))}
      </select>
    );
  }
);

describe("the application", () => {
  it("renders events once request is", async () => {
    // global.fetch = fetchEvents
    const { findByText, findAllByText, getByText } = render(<App />);
    getByText(/Sorry no events could be found/i);
    await findByText(/Wednesday, 4th December 2019/i);
    await findAllByText(/Doors open at 20:00/i);
    await findAllByText(/London/i);
  });
  it("Filters by Country", async () => {
    // global.fetch = fetchEvents
    const { getByTestId, getByText } = render(<App />);

    const citySelect = getByTestId(/select/i);

    fireEvent.change(citySelect, { target: { value: "oslo" } });
    const Button = getByText(/Search/i).closest("button");
    fireEvent.click(Button);

    const eventList = getByTestId(/EventsList/i);

    // await the first element due to api request, this throws if element is not found
    await findAllByTextDom(eventList, /Oslo/i);

    /// query here as these don't throw but return empty array
    expect(queryAllByTextDom(eventList, /London/i)).toEqual([]);
    expect(queryAllByTextDom(eventList, /NYC/i)).toEqual([]);
    expect(queryAllByTextDom(eventList, /Brighton/i)).toEqual([]);
    expect(queryAllByTextDom(eventList, /Oxford/i)).toEqual([]);
    expect(queryAllByTextDom(eventList, /Leeds/i)).toEqual([]);
  });
  it("Filters by date and can reset filter", async () => {
    // global.fetch = fetchEvents
    const { getByText, findByText, getByLabelText, queryByText } = render(
      <App />
    );

    const date = getByLabelText(/Filter By Date/i);

    fireEvent.change(date, { target: { value: "2019-12-04" } });

    const Button = getByText(/Search/i).closest("button");
    fireEvent.click(Button);

    // await the first element due to api request, this throws if element is not found
    await findByText(/Wednesday, 4th December 2019/i);
    // query here as this does not throw
    expect(queryByText(/Thursday, 5th December 2019/i)).toEqual(null);

    const Reset = getByText(/Reset Filter/i).closest("button");
    fireEvent.click(Reset);

    // these throws if element is not found
    getByText(/Wednesday, 4th December 2019/i);
    getByText(/Thursday, 5th December 2019/i);
  });
});
