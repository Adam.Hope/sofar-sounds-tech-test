# Sofar Sounds Tech Test 

![App Image](./public/ScreenshotApp.png)

## `What I have included`

- I decided to build this simple app using 
  - create-react-app
  - styled components
  - flow
  - jest
  - react testing library

-  I opted for keeping state as simple as possible using React hooks and passing state down via props.  (For a larger application I might opt for a state management library like Redux but this seemed to fit the purpose for this test)
- I have used varying CSS techniques on this page to show a variety of methods to style the page including CSS grid, Flex and absolute positioning.

## `To Do`

- I used Google Material UI components for the select and date picker to speed up development.  For this reason the UI doesn't exactly match the design as they are a pain to re-style.  With more time I would implement my own following the design.
- I don't have as many tests as I would of liked but I tried to make sure I got integration tests that render the entire app and ensure events are rendered and the country, date and reset filters are tested.  Given more time I would consider adding unit tests and some snapshot tests.
- Currently I render the "no events" message before the api request.  Ideally I would have a loading state here
- Use context api to connect state directly where it is required
- Add a pre commit hook that lints, runs tests and runs prettier
- Add E2E tests with Cypress
- Improve responsive design to work on common device sizes
- Add more common spacing and sizing into the theme

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
